% parameters
disp('setting parameters');
images_dir = '/home/haseeb/resources/data/CelebA/Img/img_align_celeba/';
images_partition_file = '/home/haseeb/resources/data/CelebA/Eval/list_eval_partition.txt';
images_attr_file = '/home/haseeb/resources/data/CelebA/Anno/list_attr_celeba_raw.txt';
attribute_names = {'5_o_Clock_Shadow', 'Arched_Eyebrows', 'Attractive', 'Bags_Under_Eyes', 'Bald', 'Bangs', 'Big_Lips', 'Big_Nose', 'Black_Hair', 'Blond_Hair', 'Blurry', 'Brown_Hair', 'Bushy_Eyebrows', 'Chubby', 'Double_Chin', 'Eyeglasses', 'Goatee', 'Gray_Hair', 'Heavy_Makeup', 'High_Cheekbones', 'Male', 'Mouth_Slightly_Open', 'Mustache', 'Narrow_Eyes', 'No_Beard', 'Oval_Face', 'Pale_Skin', 'Pointy_Nose', 'Receding_Hairline', 'Rosy_Cheeks', 'Sideburns', 'Smiling', 'Straight_Hair', 'Wavy_Hair', 'Wearing_Earrings', 'Wearing_Hat', 'Wearing_Lipstick', 'Wearing_Necklace', 'Wearing_Necktie', 'Young'};
frac = 0.2;

vgg_orig_net_path = '/home/haseeb/resources/data/models/pretrained/matlab/imagenet-vgg-f.mat';

workspace_dir = strcat('/home/haseeb/school/study/ir/project/workspace/data_', num2str(frac) , '/');
vgg_input_data_path = strcat(workspace_dir,'celeba_input_imagenet_vgg.mat');
