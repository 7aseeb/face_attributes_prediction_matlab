% start clean
clear;

% reading parameters
set_parameters
tuning_out_dir = strcat(workspace_dir,'vgg_tuning');
tuning_epocs = 15;
tuning_batch = 100;
tuning_learning_rate = 0.0001;

%%
disp('fine-tuning vgg-f');
fine_tune_vgg;
