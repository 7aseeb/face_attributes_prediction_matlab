tic

% load the data
disp('loading data ...');
load(vgg_input_data_path); % loads celeba

% train options
trainOpts.expDir = tuning_out_dir ;
%trainOpts.gpus = [] ;
trainOpts.gpus = [1] ;
trainOpts.batchSize = tuning_batch;
trainOpts.learningRate = tuning_learning_rate ;
trainOpts.plotDiagnostics = false ;
%trainOpts.plotDiagnostics = true ; % Uncomment to plot diagnostics
trainOpts.numEpochs = tuning_epocs ;
trainOpts.errorFunction = 'binary';
trainOpts.continue = true ;

% load the pretrained model
net = load(vgg_orig_net_path);

% remove the softmax and the last fc layers and add a new fc layer suitable for the new task
net.layers = net.layers(1:end-2);
net.layers{end+1} = struct(...
  'name', 'dropout', ...
  'type', 'dropout', ...
  'rate', 0.5) ;
net.layers{end+1} = struct(...
  'name', 'prediction', ...
  'type', 'conv', ...
  'weights', {xavier(1,1,4096,40)}, ...
  'pad', 0, ...
  'stride', 1) ;

% add loss layer
net.layers{end+1} = struct('type', 'loss') ;

% % fine tune
net = cnn_train(net, celeba, @getBatch, trainOpts) ;

toc

%% evaluate the model
disp('evaluating the tuned net');
%remove loss layer
%net.layers(22) = [] ;

% calculate the attributes prediction error for each image
correct_predictions = 0;
% find all test images
test_imgs_indices = find(celeba.images.set == 2);
test_imgs_number = numel(test_imgs_indices);
for img_ind = test_imgs_indices(5)
    % resize the image
    im_ = imresize(celeba.images.raw.data{img_ind}, celeba.images.meta.size(1:2));
    imagesc(im_);
    set(gca,'xtick',[]);
set(gca,'xticklabel',[]);
    set(gca,'ytick',[]);
set(gca,'yticklabel',[]);
    % subtract the training mean, after converting to a single precision
    im_ = single(im_) - celeba.images.meta.avg;
    % evaluate the network
    res = vl_simplenn(net, im_);
    predicted_attrs = transpose(res(end).x(:));
    labels = celeba.images.raw.labels(img_ind, :);
    for i = 1:numel(predicted_attrs)
        if (labels(i) == sign(predicted_attrs(i)))
            result = 'correct';
        else
            result = 'incorrect';
        end
        disp([attribute_names{i}, ' : ', result, ' : ', num2str(labels(i))]);
    end
%     [~, predicted_attrs] = max([-predicted_attrs/2; predicted_attrs/2]);
%     predicted_attrs(predicted_attrs ==1)= -1;
%     predicted_attrs(predicted_attrs ==2)= 1;
    % calculate the error and add it to the total error
    %correct_predictions = correct_predictions + sum(predicted_attrs == celeba.images.raw.labels(img_ind, :));
      correct_predictions = correct_predictions + sum(sign(predicted_attrs) == celeba.images.raw.labels(img_ind, :));
    %correct_predictions = correct_predictions + sum(argmax(predicted_attrs/2, -predicted_attrs/2) == celeba.images.raw.labels(img_ind, :));
end
% calculate and print the accuracy
accuracy = correct_predictions/(40 * test_imgs_number);
disp(['test accuracy : ', num2str(accuracy)]);
