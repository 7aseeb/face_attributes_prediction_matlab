% start clean
clear;

% reading parameters
set_parameters
%target_net_path = vgg_orig_net_path;
target_net_path = '/nest/school/study/ir/project/workspace/data_0.2/vgg_tuning/net-epoch-15.mat';
orig_vgg_net = 0; % 1 for orig vgg net 0 if some other net is used
layer = 16;
features_out_filename = strcat( workspace_dir, 'vgg_', num2str(orig_vgg_net),'_', num2str(layer),'_features.mat');
lambda = 0.0001;
epocs = 20;
attributes_ids = (1:40);

%% Extract CNN features
disp('Extracting CNN features');
extract_store_cnn_features

%% Training/evaluating attributes predictors 
disp('Training/evaluating attributes predictors ');
train_evaluate_attribute_predictor