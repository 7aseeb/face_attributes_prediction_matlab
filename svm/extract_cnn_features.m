function cnn_features = extract_cnn_features(celeba, net, layer, orig_vgg_net)
n = size(celeba.images.raw.data,2);
info = vl_simplenn_display(net);
d = info.dataSize(1,layer+1) * info.dataSize(2,layer+1) * info.dataSize(3,layer+1);
cnn_features = zeros(d, n);

for i = 1:n
    % preprocess the image
    im_ = single(celeba.images.raw.data{i}) ; % note: 0-255 range
    im_ = imresize(im_, celeba.images.meta.size(1:2)) ;
    if(orig_vgg_net == 1)
        im_ = im_ - net.meta.normalization.averageImage;
    else
        im_ = im_ - celeba.images.meta.avg ;
    end
    % evaulate the CNN
    res = vl_simplenn(net, im_);
    cnn_features(:,i) = squeeze(gather(res(layer+1).x(:)));
end

end