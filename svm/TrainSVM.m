function [w, b] = TrainSVM(Xtrain, ys, lambda, epocs)
[d,n] = size(Xtrain);
% init w and b
w = zeros(d,1);
b = 0;
% training
disp('training started ...');
for epoc = 1:epocs
    disp(['epoc : ', num2str(epoc)]);
    for i_t = randperm(n)
        t = (epoc - 1) * n + i_t;
        eta_t = 1 / (lambda * t);
        if (transpose(w) * Xtrain(:,i_t) + b) * ys(i_t, 1) < 1
            w = (1 - eta_t * lambda) * w + (eta_t * ys(i_t, 1)) * Xtrain(:,i_t);
            b = b + eta_t * ys(i_t, 1);
        else
            w = (1 - eta_t * lambda) * w;
        end
    end
end
disp('training finished ...');

end