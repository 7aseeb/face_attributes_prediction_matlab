function [y_predicted, accuracy] = predict(w, b, data, y_truth)
y_predicted = sign(transpose(w) * data + b);
comparisons = transpose(y_predicted) == y_truth;
accuracy = sum(comparisons)/numel(comparisons);
end