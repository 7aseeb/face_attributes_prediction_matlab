function display_samples_prediction(data_raw, predictions, truth, number_samples, attribute_names, grid_width, grid_height)

disp('<image id> #### <attribute name> : <prediction>(<truth>)');
for i = 1:number_samples
    sample_id = randi(size(data_raw.data.images,2));
    subplot(grid_width,grid_height,i) ;
    imagesc(data_raw.data.images{sample_id});
    title(num2str(sample_id));
    axis image off;
    disp(get_predictions_string(sample_id, predictions(:,sample_id), truth(:,sample_id), attribute_names));
end
end

function prediction_string = get_predictions_string(id, predictions,truth, attribute_names)
prediction_string = sprintf('%d ### ', id);
for i = 1:numel(predictions)
    prediction_string = strcat(prediction_string, sprintf('%s : %d(%d)',attribute_names{i},predictions(i),truth(i)));
end
end