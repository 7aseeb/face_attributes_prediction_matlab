tic

% input data
disp('loading data ...');
load(vgg_input_data_path); % loads celeba

% load the model
disp('loading pretrained cnn model ...');
net = load(target_net_path);
onet = net;
if isfield(net, 'net')
    net = net.net;
    net.layers(22) = [] ; %%%%%%%%%%%%%%%%%%%%%%%%%
end
net = vl_simplenn_tidy(net) ;

% extract cnn featrures
disp('cnn features exraction ...');
data.features = extract_cnn_features(celeba, net, layer, orig_vgg_net);
data.labels = celeba.images.raw.labels;
data.set = celeba.images.set;

% saving to disk
disp('saving extracted features to disk ...');
save(features_out_filename, 'data', '-v7.3');

toc