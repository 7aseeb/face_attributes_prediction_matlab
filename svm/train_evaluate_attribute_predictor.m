tic

% load features
disp('loading features');
load(features_out_filename);
train_index = find(data.set(1,:) ==  1);
test_index = find(data.set(1,:) ==  2);
Xtrain = data.features(:,train_index);
Xtest =  data.features(:,test_index);
train_attrs = data.labels(train_index, :);
test_attrs = data.labels(test_index, :);

% training
models = {};
for i = 1:numel(attributes_ids)
    disp(['training attribute : ', attribute_names{i}]);
    Ytrain = train_attrs(:,attributes_ids(i));
    [models{i}.w, models{i}.b] = TrainSVM(Xtrain, Ytrain, lambda, epocs);
    models{i}.id = attributes_ids(i);
end

toc

% validation
tic
accuracy_train_total= 0;
accuracy_test_total= 0;
number_models = size(models,2);
predictions_test = zeros(number_models, size(Xtest,2));
attribute_ids = zeros(number_models,1);
for i = 1:number_models
    disp('--------------------------------------------------------');
    model = models{1,i};
    disp(['evaluating model for : ', attribute_names{model.id}]);
    % accuracy in training data
    [~, accuracy] = predict(model.w, model.b, Xtrain, train_attrs(:,model.id));
    disp(['Training data accuracy : ', num2str(accuracy)]);
    accuracy_train_total = accuracy_train_total + accuracy;
    
    % accuracy in test data
    [predictions_test(i,:), accuracy] = predict(model.w, model.b, Xtest,  test_attrs(:,model.id));
    disp(['Test data accuracy : ', num2str(accuracy)]);
    accuracy_test_total = accuracy_test_total + accuracy;
    disp('--------------------------------------------------------');
    attribute_ids(i) = model.id;
end

disp(['overall train accuracy : ', num2str(accuracy_train_total/ number_models)]);
disp(['overall test accuracy : ', num2str(accuracy_test_total/ number_models)]);

toc

% show sample predictions
%display_samples_prediction(test_raw, predictions_test, transpose(test_attrs(:,attribute_ids)), 9, attribute_names, 3, 3);