function avg_im = calc_avg_img(images)
n = numel(images);
avg_im =  zeros(size(images{1}));
for i = 1:n
    avg_im = avg_im * (i-1)/i + double(images{i})/i;
end

end