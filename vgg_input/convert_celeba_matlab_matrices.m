tic

% start clean
clear;

% reading parameters
set_parameters
mkdir(workspace_dir); % create the workspace dir if not there

% load dataset given partition
partition = load(images_partition_file);
train_index = find(partition(:,2) ==  0);
test_index = find(partition(:,2) ==  2);
total_train_size = numel(train_index);
total_test_size = numel(test_index);

% suffle the indices
train_index = train_index(randperm(total_train_size));
test_index = test_index(randperm(total_test_size));

% select subset of the data
train_index = train_index(1:frac*total_train_size,:); train_index = train_index(:);
test_index = test_index(1:frac*total_test_size,:); test_index = test_index(:);
all_imagefiles = dir(strcat(images_dir, '*.jpg'));
train_image_files = all_imagefiles(train_index);
test_image_files = all_imagefiles(test_index);

% select corresonding attributes subset
all_attributes = load(images_attr_file);
train_attrs = all_attributes(train_index,2:41);
test_attrs = all_attributes(test_index,2:41);

% create .mat data files
store_matrix_disk(images_dir, train_image_files, train_attrs, strcat(workspace_dir, 'train.mat'));
store_matrix_disk(images_dir, test_image_files, test_attrs, strcat(workspace_dir, 'test.mat'));

% clear
clear;

toc