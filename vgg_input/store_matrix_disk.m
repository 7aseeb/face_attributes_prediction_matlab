function store_matrix_disk(images_dir, imagefiles, attrs, matfilename)
nfiles = length(imagefiles);   % Number of image files
disp(['creating ', matfilename, ' ... using ', int2str(nfiles), ' files']);

data.images = {};
data.attributes = attrs;

for ii=1:nfiles
   currentfilename = strcat(images_dir, imagefiles(ii).name);
   currentimage = imread(currentfilename);
   data.images{ii} = currentimage;
end

% save to disk
save(matfilename, 'data', '-v7.3');
end