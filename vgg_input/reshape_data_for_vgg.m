tic
% start clean
clear;

% reading parameters
set_parameters

% input data
disp('loading data ...');
train_data_file = strcat(workspace_dir,'train.mat');
test_data_file = strcat(workspace_dir,'test.mat');
train = load(train_data_file);
test = load(test_data_file);

% get vgg input size
net = vl_simplenn_tidy(load(vgg_orig_net_path)) ;
celeba.images.meta.size = net.meta.normalization.imageSize();

% create avg image
disp('calculating mean image');
celeba.images.meta.avg = single(imresize(calc_avg_img(train.data.images), celeba.images.meta.size(1:2)));

% create train test split
ntrain = size(train.data.images, 2);
ntest = size(test.data.images, 2);
trainset = ones(1,ntrain);
testset = ones(1,ntest)+1;
celeba.images.set = single(cat(2, trainset, testset));

% store the images and labels
celeba.images.raw.data = [train.data.images test.data.images];
celeba.images.raw.labels = single(cat(1, train.data.attributes, test.data.attributes));

% save to disk
disp('saving vgg input data to disk');
save(vgg_input_data_path, 'celeba', '-v7.3');

toc