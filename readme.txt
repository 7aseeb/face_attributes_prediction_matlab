To train and evaluate an atrribute predictor do:

1) convert part of celebA into format suitable for vgg (only subset of celebA is converted). Check set_parameters.m and below script for used parameters.
>> create_vgg_input

2) initialize MatConvNet.
>> init_matconvnet

3) Evaluate the attribute predictors (SVMs) before fine tuning. In below script, set target_net_path = <orig vgg net path> and orig_vgg_net = 1.
>> train_validate_attrs_predictors

4) Fine-tune vgg-f using celeba.  Check set_parameters.m and below script for used parameters.
>> tune_vgg

5) Evaluate the attribute predictors (SVMs) after fine tuning. In below script, set target_net_path = <tuned vgg net path> and orig_vgg_net = 0.
>> train_validate_attrs_predictors